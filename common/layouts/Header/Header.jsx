/* eslint-disable react/prop-types */
import React from 'react';

export const Header = ({ children }) => (
  <>
    {children}
  </>
);

export default Header;
