/* eslint-disable no-unused-vars */
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

const LoginRequired = ({ children }) => {
  const { isAuth } = useSelector((data) => data);
  const router = useRouter();

  // useEffect(() => {
  //   if (isAuth) {
  //     router.push('/');
  //   }
  //   router.push('/login');
  // }, [isAuth]);

  return (
    <>
      {children}
    </>
  );
};

LoginRequired.propTypes = {
  children: PropTypes.object.isRequired,
};

export default LoginRequired;
