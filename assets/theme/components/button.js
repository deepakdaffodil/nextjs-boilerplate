const buttonStyles = {
  styleOverrides: {
    root: {
      borderRadius: '21px',
    },
    containedPrimary: {
      color: 'white',
      textTransform: 'capitalize',
    },
  },
};

export default buttonStyles;
