import Header from '../common/layouts/Header';
import LoginRequired from '../common/routeGuard/LoginRequired';
import Home from '../views/Home';

const home = () => null;
home.View = Home;
home.Layout = Header;
home.RouteGuard = LoginRequired;

export default home;
