import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  appBar: {
    background: 'none',
    boxShadow: 'none',
    color: '#fff',
  },
  singInButton: {
    marginLeft: '20px',
  },
  menu: {
    display: 'flex',
  },
  headerBlankDiv: {
    flexGrow: 1,
  },
  menuItem: {
    padding: '10px',
    marginLeft: '20px',
  },
});

export default useStyles;
