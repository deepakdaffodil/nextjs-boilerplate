import { Grid, Typography } from '@mui/material';
import PropTypes from 'prop-types';
import Image from 'next/image';
import React from 'react';
import useStyles from './styles';

export default function SectionOne({ data, currIndex }) {
  const reverse = currIndex % 2 !== 0;
  const classes = useStyles();

  return (
    <Grid container className={!reverse ? classes.root : classes.rootReverse}>
      <Grid
        md={6}
        item
        className={
          !reverse ? classes.imageContainerLeft
            : classes.imageContainerRight
        }
      >
        <Image src={`/${data.img}`} width="400%" height="400%" />
      </Grid>
      <Grid item md={6} className={!reverse ? classes.textContainer : classes.textContainerReverse}>
        <Typography variant="h4" fontWeight="bold">
          {data.heading}
        </Typography>
        <Typography variant="body1" color="primary">
          {data.description}
        </Typography>
      </Grid>
    </Grid>
  );
}

SectionOne.defaultProps = {
  data: {
    img: 'mobile1.png',
    heading: 'NA',
    description: 'NA',
  },
  currIndex: 0,
};

SectionOne.propTypes = {
  data: PropTypes.object,
  currIndex: PropTypes.number,
};
