import { makeStyles } from '@mui/styles';

const useStyles = makeStyles(
  (theme) => ({
    root: {
      minHeight: '100vh',
      backgroundImage: "url('/banner.jpg')",
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
    },
    sectionOne: {
      padding: '30px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      backgroundColor: '#fcfbf8',
    },
    rectangle: {
      width: '100px',
      padding: '5px',
      marginTop: '30px',
      marginBottom: '10px',
      backgroundColor: '#d1a85e',
      borderRadius: '10px',
    },
    rectangleBlack: {
      width: '100px',
      padding: '5px',
      marginTop: '30px',
      marginBottom: '10px',
      backgroundColor: '#000',
      borderRadius: '10px',
    },
    serviceCardContainer: {
      display: 'flex',
      padding: '5px',
      margin: '20px',
      justifyContent: 'space-between',
    },
    sectionTwo: {
      backgroundColor: '#faf8f2',
      color: '#000',
      padding: '30px',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      [theme.breakpoints.down('md')]: {
        padding: 0,
      },
    },
    merchantCardContainer: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'center',
      margin: '0 auto',
    },
    startButtonContainer: {
      width: '18%',
      height: 40,
      display: 'flex',
      justifyContent: 'center',
      marginTop: 20,
    },
    sectionThree: {
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between',
      padding: 10,
      backgroundColor: '#faf8f3',
    },
    sectionThreeDescription: {
      width: '60%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      textAlign: 'left',
      paddingLeft: '50px',
      [theme.breakpoints.down('md')]: {
        width: '100%',
        padding: 10,
        margin: 0,
      },
    },
    sectionThreeImage: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end',
      [theme.breakpoints.down('md')]: {
        justifyContent: 'center',
      },
    },
    sectionThreeSocial: {
      width: '50%',
      display: 'flex',
      justifyContent: 'space-between',
      [theme.breakpoints.down('md')]: {
        width: '60%',
      },
      [theme.breakpoints.down('sm')]: {
        width: '80%',
      },
    },
  }),
  { name: 'HomeMuiCustomStyle' },
);

export default useStyles;
