import React from 'react';
import { Button, Typography, Grid } from '@mui/material';
import Image from 'next/image';
import Icon from '../../common/components/icons';
import PaymentCard from './components/PaymentCard';
import { paymentCard, sectionOneData } from './utils';
import SectionOne from './components/SectionOne';
import Header from './components/Header/Header';
import Banner from './components/Banner';
import Footer from './components/Footer';
import useStyles from './styles';

function Home() {
  const classes = useStyles();
  return (
    <>
      <div className={classes.root}>
        <Header />
        <Banner />
      </div>
      {sectionOneData.map((data, index) => (
        <SectionOne key={data.heading} currIndex={index} data={data} />
      ))}
      <div className={classes.sectionTwo}>
        <div className={classes.rectangleBlack} />
        <Typography
          variant="h4"
          color="#000"
          textAlign="center"
          fontWeight="bold"
          fontFamily="rubik"
        >
          Our Payment Options
        </Typography>
        <Grid container spacing={4} padding={5}>
          {paymentCard.map((data) => (
            <PaymentCard key={data.description} data={data} />
          ))}
        </Grid>
      </div>
      <div className={classes.sectionOne}>
        <div className={classes.rectangle} />
        <Typography
          variant="h4"
          color="initial"
          textAlign="center"
          fontWeight="bold"
          fontFamily="rubik"
        >
          Partner Merchants
        </Typography>
        <Typography
          variant="body1"
          color="primary"
          textAlign="center"
          marginTop={5}
        >
          We offer several shops where you can pay securely with your Betoline
          <br />
          account and benefit from several promotions, discounts and bonuses.
        </Typography>
      </div>
      <div className={classes.sectionOne}>
        <div className={classes.rectangle} />
        <Typography
          variant="h4"
          color="initial"
          textAlign="center"
          fontWeight="bold"
          fontFamily="rubik"
        >
          Become a Merchant
        </Typography>
        <Typography
          variant="body1"
          color="primary"
          textAlign="center"
          marginTop={5}
        >
          Do you have a website, a physical store or a mobile application? Are
          you looking for a secure payment kit that includes all payment methods
          in Tunisia? Do you want to offer your products to thousands of users
          registered on the EL-Wallet platform?
        </Typography>
        <Typography
          variant="body1"
          color="primary"
          textAlign="center"
          marginTop={5}
        >
          So opt for our “EL-Wallet Merchant Kit” solution which allows you to
          be paid in complete security without worrying about payment problems,
          fraud or customer refunds.
        </Typography>
        <div className={classes.startButtonContainer}>
          <Button variant="contained" type="primary" fullWidth>
            Start
          </Button>
        </div>
      </div>
      <Grid container className={classes.sectionThree}>
        <Grid md={6} item className={classes.sectionThreeDescription}>
          <Typography variant="h4" fontWeight="bold">
            Get App “EL-Wallet"
          </Typography>
          <Typography variant="body1" color="primary">
            Receive money, send money home, pay bills, buy groceries from your
            local store, pay for bus tickets via EL-Wallet and transfer money to
            family and friends. Everything in a single app, EL-Wallet at your
            fingertips.
          </Typography>
          <div className={classes.sectionThreeSocial}>
            <Image src={Icon.appstore} width="100%" height="100%" />
            <Image src={Icon.playstrore} width="100%" height="100%" />
          </div>
        </Grid>
        <Grid item md={6} className={classes.sectionThreeImage}>
          <Image src="/mobile2.png" width="400%" height="400%" />
        </Grid>
      </Grid>
      <Footer />
    </>
  );
}

export default Home;
